This website contains [Mermaid](https://mermaid-js.github.io/mermaid/#/) flow charts cataloging walkthroughs of various penetration testing scenarios.

{{< mermaid >}}
graph LR
    A[Find the box] --> B[pwn the box]
{{</ mermaid >}}